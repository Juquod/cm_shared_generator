#!/usr/bin/python3

import unittest
import tests as test_module

def discover_and_run_tests():
	loader = unittest.defaultTestLoader
	runner = unittest.TextTestRunner()
	tests_suite = loader.loadTestsFromModule(test_module)
	runner.run(tests_suite)

if __name__ == '__main__':

	discover_and_run_tests()